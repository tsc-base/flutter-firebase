# Dockerコンテナ基本イメージ: Flutter + Firebase + NodeJS
FROM registry.gitlab.com/tsc-base/node:@ver@

ENV DEBIAN_FRONTEND=noninteractive

# バージョン指定
ARG flutter_ver=3.3.4

# 依存ファイルインストール
RUN sh -c 'wget -qO- https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -'; \
    sh -c 'wget -qO- https://storage.googleapis.com/download.dartlang.org/linux/debian/dart_stable.list > /etc/apt/sources.list.d/dart_stable.list'; \
    apt-get update; \
    apt-get upgrade; \
    # Dart Install
    apt install dart; \
    apt install apt-transport-https; \
    # Clean up
    apt-get autoremove -y; \
    apt-get clean -y; \
    rm -rf /var/lib/apt/lists/*

ENV PATH $PATH:PATH:/usr/lib/dart/bin

# Install Flutter

ENV FLUTTER_HOME=/usr/local/flutter \
    FLUTTER_VERSION=${flutter_ver} \
    PATH=$PATH:/usr/local/flutter/bin

RUN apt-get update \
 && apt-get upgrade -y \
 && apt-get install -y --no-install-recommends --no-install-suggests ca-certificates \
 && update-ca-certificates \
    \
 && apt-get install -y --no-install-recommends --no-install-suggests \
            build-essential \
            clang cmake \
            lcov \
            libgtk-3-dev liblzma-dev \
            ninja-build \
            pkg-config \
            \
 && curl -fL -o /tmp/flutter.tar.xz https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_${flutter_ver}-stable.tar.xz \
 && tar -xf /tmp/flutter.tar.xz -C /usr/local/ \
 && git config --global --add safe.directory /usr/local/flutter \
 && flutter config --enable-web --no-enable-android --no-enable-ios \
 && flutter precache --web --no-android --no-ios \
 && flutter --version \
    \
 && rm -rf /var/lib/apt/lists/* /tmp/*

# Install Firebase CLI

RUN npm install -y -g yarn \
 && yarn global add firebase-tools \
 && dart pub global activate flutterfire_cli \
 && echo 'PATH="$PATH:$HOME/.pub-cache/bin"' >> ~/.bashrc

ENV DEBIAN_FRONTEND=dialog